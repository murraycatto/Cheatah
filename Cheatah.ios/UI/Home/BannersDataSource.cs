﻿using System;
using Alliance.Carousel;
using UIKit;
using CoreGraphics;
using System.Collections.Generic;
using SDWebImage;
using Foundation;
using Cheatah.shared;
using System.Timers;
using Newtonsoft.Json.Linq;

namespace Cheatah.ios
{
	public class BannersDataSource : CarouselViewDataSource
	{
		Home vc;
		nfloat deviceWidth;
		List<GenericModel> model;

		public BannersDataSource(Home vc, nfloat deviceWidth, List<GenericModel> model)
		{
			this.vc = vc;
			this.deviceWidth = deviceWidth;
			this.model = model;
		}

		public override nint NumberOfItems(CarouselView carousel)
		{
			return (nint)model.Count;
		}

		public override UIView ViewForItem(CarouselView carousel, nint index, UIView reusingView)
		{
			reusingView = new UIView(new CGRect(0, 0, deviceWidth, 120));
			UIImageView banner = new UIImageView(new CGRect(0, 0, deviceWidth, 120))
			{
				ContentMode = UIViewContentMode.ScaleAspectFill,
				BackgroundColor = UIKit.UIColor.Clear,
			};
			banner.SetImage(
				url: new NSUrl(model[int.Parse(index + "")].Image)
			);
			reusingView.Add(banner);
			return reusingView;
		}

	}

}

