using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using System.Collections.Generic;
using Alliance.Carousel;
using CoreGraphics;
using Newtonsoft.Json.Linq;
using BigTed;
using Cheatah.shared;

namespace Cheatah.ios {
	public partial class Home : UIViewController {
		public Home(IntPtr handle) : base(handle) {
		}

		Helper helper = new Helper();
		API api = new API();

		nfloat newY = 0;
		nfloat deviceWidth;
		nfloat deviceHeight;
		List<GenericModel> model = new List<GenericModel> { };
		CarouselView carouselBanners;
		UIScrollView scrollView;
		List<GameModel> Games = new List<GameModel> { };
		System.Timers.Timer timer;
		UITableView tbl;
		UIView howToHolder;
		UIHelper uihelper;
		public override void ViewDidLoad() {
			base.ViewDidLoad();
			uihelper = new UIHelper();
			deviceWidth = this.View.Bounds.Width;
			deviceHeight = this.View.Bounds.Height;
			this.NavigationItem.Title = "Home";

			scrollView = new UIScrollView(new CGRect(0, 0, deviceWidth, deviceHeight));
			View.Add(scrollView);
			View.BackgroundColor = AppConstants.Grey;
			tbl = new UITableView(new CGRect(0, 140, deviceWidth, deviceHeight - 205));
			tbl.SeparatorStyle = UITableViewCellSeparatorStyle.None;

			howToHolder = new UIView(new CGRect(10, 140, deviceWidth - 20, deviceHeight - 215));
			howToHolder.BackgroundColor = UIColor.White;
			howToHolder.Layer.ShadowColor = UIColor.DarkGray.CGColor;
			howToHolder.Layer.ShadowOpacity = 1.0f;
			howToHolder.Layer.ShadowRadius = 6.0f;
			howToHolder.Layer.CornerRadius = 5;
			howToHolder.Layer.ShadowOffset = new System.Drawing.SizeF(0f, 3f);
			howToHolder.Layer.MasksToBounds = false;


			if(String.IsNullOrEmpty(helper.getUUID())) {
				addUUID();
			}
			if(String.IsNullOrEmpty(helper.getBanners())) {
				getBanners();
			} else {
				addBanners();
				updateBanners();
			}
			if (String.IsNullOrEmpty(helper.getFavorite()))
			{
				getFavorites();
			}
			else {
				addFavs();
				updateFavorties();
			}

			setupTimer();
		}
		public override void ViewWillAppear(bool Animated) {
			base.ViewWillAppear(Animated);
			timer.Enabled = true;

			Console.WriteLine(this.Title.ToUpper());
			this.NavigationController.InteractivePopGestureRecognizer.Enabled = true;
			if (String.IsNullOrEmpty(helper.getFavorite()))
			{
				getFavorites();
			}
			else {
				addFavs();
				updateFavorties();
			}
		}
		public override void ViewDidAppear(bool animated) {
			base.ViewDidAppear(animated);
			this.NavigationController.NavigationBar.TintColor = AppConstants.Primary;
			UIImage imgPlatforms = new UIImage("platform.png");
			imgPlatforms = imgPlatforms.Scale(new CGSize(40, 33));
			this.NavigationItem.SetRightBarButtonItem(
				new UIBarButtonItem(imgPlatforms
					, UIBarButtonItemStyle.Plain
					, (sender, args) =>
					{
						Platforms controller = Storyboard.InstantiateViewController("Platforms") as Platforms;
						NavigationController.PushViewController(controller, true);
					})
				, true);
		}
		public override void ViewWillDisappear(bool animated) {
			base.ViewWillDisappear(animated);
			timer.Enabled = false;
		}
		void addUUID() {
			var guid = System.Guid.NewGuid();
			helper.setUUID(guid.ToString());
		}
		#region
		public async void updateFavorties() {
			string message = AppConstants.GenericError;
			try {
				message = await api.GetFav();
				addFavs();
			} catch(Exception e) {
				Console.WriteLine(e.Message);
			}
		}
		public async void getFavorites() {
			var bounds = UIScreen.MainScreen.Bounds;
			LoadingOverlay loadingOverlay = new LoadingOverlay (bounds,"Please wait ...");
			View.Add (loadingOverlay);
			string message = AppConstants.GenericError;
			try {
				message = await api.GetFav();
				addFavs();
			} catch(Exception e) {
				Console.WriteLine(e.Message);
			}
			if(message == "0") {
				loadingOverlay.Hide ();
			} else {
				loadingOverlay.Hide ();
			}
		}
		public void addFavs() {
			try {
				Games.Clear();
				JObject json = JObject.Parse(helper.getFavorite());
				foreach(JObject j in json["games"]) {
					Games.Add(
						new GameModel(
							int.Parse(j["id"].ToString()),
							j["name"].ToString(),
							j["name2"].ToString(),
							j["image"].ToString(),
							j["banner"].ToString(),
							j["info"].ToString(),
							j["company"].ToString(),
							int.Parse(j["platformid"].ToString()),
							j["platform_name"].ToString(),
							j["platform_image"].ToString()
						)
					);
				}
				if(Games.Count == 0) {
					tbl.RemoveFromSuperview();
					addHowTo();
				} else {
					howToHolder.RemoveFromSuperview();
					scrollView.AddSubview(tbl);
					tbl.Source = new GamesTableSource(Games, this);
					tbl.ReloadData();
				}
			} catch(Exception e) {
				Console.WriteLine(e.Message);
				BTProgressHUD.ShowImage(UIImage.FromBundle("error"), AppConstants.GenericError, 500);
			}
		}

		public async void DeleteGame(GameModel g) {
			string message = AppConstants.GenericError;
			try {
				message = await api.DeleteFav(g.PlatformID, g.ID);
			} catch(Exception e) {
				Console.WriteLine(e.Message);
			}
			if(message == "0") {
			} else {
				var bounds = UIScreen.MainScreen.Bounds;
				LoadingOverlay loadingOverlay = new LoadingOverlay (bounds,"Please wait ...");
				View.Add (loadingOverlay);
				BTProgressHUD.ShowImage(UIImage.FromBundle("error"), message, 500);
				loadingOverlay.Hide (2, true);
			}
			updateFavorties();
		}
		#endregion

		public void addHowTo() {

			newY = 20;
			#region
			UILabel lblHowTo = new UILabel(new CGRect(10, newY, deviceWidth - 20, 35));
			lblHowTo.Text = "How To";
			lblHowTo.Font = lblHowTo.Font.WithSize(22);
			lblHowTo.Font = AppConstants.VerdanaBold;
			lblHowTo.TextAlignment = UITextAlignment.Left;
			lblHowTo.TextColor = UIColor.Black;
			newY = uihelper.incNewY (newY, lblHowTo);
			UIImageView line = new UIImageView(new CGRect(10, newY, 50, 2));
			line.BackgroundColor = AppConstants.Primary;
			#endregion

			#region
			UITextView txHowTo = new UITextView(new CGRect(8, newY + 10, deviceWidth - 40, 150));
			uihelper.createInfoView (txHowTo,"• First Click the button in the top corner \n• Select the platform of your choice \n• Select a game of your choice \n• Add the game to your favorites");
			txHowTo.Font = txHowTo.Font.WithSize(13);
			newY = uihelper.incNewY(newY,txHowTo,20);
			#endregion

			#region
			UIImageView star = new UIImageView(new CGRect((deviceWidth / 2) - 30, newY, 60, 60));
			star.Image = UIImage.FromBundle("star.png");
			newY = uihelper.incNewY (newY, star, 5);

			UILabel lblNoFavorites = new UILabel(new CGRect(10, newY, deviceWidth - 20, 35));
			lblNoFavorites.Text = "No Favorites Yet ...";
			lblNoFavorites.Font = AppConstants.Verdana;
			lblNoFavorites.Font = lblNoFavorites.Font.WithSize(17);
			lblNoFavorites.TextAlignment = UITextAlignment.Center;
			lblNoFavorites.TextColor = UIColor.Black;
			newY = uihelper.incNewY (newY, lblNoFavorites,50);
			#endregion
			howToHolder.AddSubview(lblHowTo);
			howToHolder.AddSubview(line);
			howToHolder.AddSubview(txHowTo);
			howToHolder.AddSubview(star);
			howToHolder.AddSubview(lblNoFavorites);
			scrollView.Add(howToHolder);
			CGSize screensize = new CGSize(deviceWidth, newY);
			scrollView.ContentSize = screensize;
		}



		#region
		public void setupTimer() {
			timer = new System.Timers.Timer();
			timer.Interval = 5000;
			timer.Elapsed += new System.Timers.ElapsedEventHandler(TimerElapsed);
			timer.Enabled = true;
		}

		protected void TimerElapsed(object sender, System.Timers.ElapsedEventArgs e) {
			nint currentBanner = carouselBanners.CurrentItemIndex;
			currentBanner++;
			if(carouselBanners.CurrentItemIndex > model.Count) {
				currentBanner = 0;
			}
			carouselBanners.ScrollToItem(currentBanner, true);
		}

		public async void getBanners() {
			var bounds = UIScreen.MainScreen.Bounds;
			LoadingOverlay loadingOverlay = new LoadingOverlay(bounds, "Please wait ...");
			View.Add(loadingOverlay);
			string message = AppConstants.GenericError;
			try {
				message = await api.GetBanners();
				addBanners();
			} catch(Exception e) {
				Console.WriteLine(e.Message);
			}
			if(message == "0") {
				loadingOverlay.Hide();
			} else {
				loadingOverlay.Hide(2, true);
			}
		}

		public void addBanners() {
			try {
				JObject json = JObject.Parse(helper.getBanners());
				foreach(JObject j in json["banners"]) {
					model.Add(new GenericModel(j["link"].ToString(), j["banner"].ToString(), 1));
				}
				carouselBanners = new CarouselView(new CGRect(0, 0, (deviceWidth), 120));
				newY = uihelper.incNewY(newY, carouselBanners, 20);
				carouselBanners.DataSource = new BannersDataSource(this, deviceWidth, model);
				carouselBanners.Delegate = new BannersDelegate(this);
				carouselBanners.CarouselType = CarouselType.Linear;
				carouselBanners.BackgroundColor = UIColor.Black;
				carouselBanners.Alpha = 0.9f;
				//carouselBanners.Autoscroll = ScrollSpeed;
				carouselBanners.PagingEnabled = true;
				carouselBanners.ConfigureView();
				carouselBanners.Bounces = true;
				scrollView.Add(carouselBanners);
				updateBanners();
			} catch(Exception ex) {
				if(AppConstants.Mode == "Beta") {
					Console.WriteLine(ex.Message);
				}
			}
		}

		public async void updateBanners() {
			string message = AppConstants.GenericError;
			try {
				message = await api.GetBanners();
			} catch(Exception e) {
				Console.WriteLine(e.Message);
			}
		}
		#endregion
	}
}
