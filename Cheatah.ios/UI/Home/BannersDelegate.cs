﻿using System;
using Alliance.Carousel;
using UIKit;

namespace Cheatah.ios
{
	public class BannersDelegate : CarouselViewDelegate
	{
		Home vc;
		public BannersDelegate(Home vc)
		{
			this.vc = vc;
		}

		public override nfloat ValueForOption(CarouselView carousel, CarouselOption option, nfloat aValue)
		{

			if (option == CarouselOption.Spacing)
			{
				return aValue * 1.1f;
			}
			if (option == CarouselOption.Wrap)
			{
				return 1.0f;
			}
			return aValue;
		}
	}
}

