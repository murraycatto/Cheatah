﻿using System;
using UIKit;
using Foundation;
using System.Collections.Generic;
using CoreGraphics;
using System.Drawing;
using SDWebImage;
using Cheatah.shared;

namespace Cheatah.ios {
	public class GenericTableSource : UITableViewSource {

		List<GenericModel> Models;
		string CellIdentifier = "TableCell";
		int TypeID;
		UIViewController VC { get; set; }
		public GenericTableSource(List<GenericModel> models, int typeid) {
			Models = models;
			TypeID = typeid;
		}
		public void SetupVC(UIViewController vc) {
			VC = vc;
		}
		public void Refresh() {

		}

		public override nint RowsInSection(UITableView tableview, nint section) {
			return Models.Count;
		}

		public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath) {
			return 80;
		}
		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath) {
			UITableViewCell cell = tableView.DequeueReusableCell(CellIdentifier);
			GenericModel model = Models[indexPath.Row];
			if(cell == null) {
				cell = new UITableViewCell(new CGRect(0, 0, tableView.Frame.Width, 70));
				cell.RestorationIdentifier = CellIdentifier;
			}
			nfloat newX = 0;
			CGRect ImageFrame = new CGRect(15, 15, 50, 50);
			UIImageView imgDownloads = (UIImageView)cell.ViewWithTag(1);
			if(imgDownloads == null) {
				imgDownloads = new UIImageView(ImageFrame);
				imgDownloads.Tag = 1;
				cell.AddSubview(imgDownloads);
			}
			imgDownloads.ContentMode = UIViewContentMode.ScaleAspectFill;
			imgDownloads.SetImage(
				url: new NSUrl(model.Image)
			);
			CGRect TextFrame = new CGRect(90, 30, tableView.Frame.Width - 70, 20);
			UILabel text = (UILabel)cell.ViewWithTag(2);
			if(text == null) {
				text = new UILabel(TextFrame);
				text.Tag = 2;
				cell.AddSubview(text);
			}
			text.Text = model.Name;
			text.Font = AppConstants.VerdanaBold;

			text.TextColor = AppConstants.Secondary;
			UIImageView line = (UIImageView)cell.ViewWithTag(3);
			if(line == null) {
				line = new UIImageView(new CGRect(0, 78, tableView.Frame.Width, 2));
				line.Tag = 3;
				line.BackgroundColor = AppConstants.Grey;
				cell.AddSubview(line);
			}
			CGRect ImageArrowFrame = new CGRect(tableView.Frame.Width - 20, 35, 15, 15);
			UIImageView imgArrowDownloads = (UIImageView)cell.ViewWithTag(4);
			if(imgArrowDownloads == null) {
				imgArrowDownloads = new UIImageView(ImageArrowFrame);
				imgArrowDownloads.Tag = 4;
				cell.AddSubview(imgArrowDownloads);
			}
			imgArrowDownloads.ContentMode = UIViewContentMode.ScaleAspectFill;
			imgArrowDownloads.Image = UIImage.FromBundle("arrow_right.png");
			return cell;
		}
		public override void RowSelected(UITableView tableView, NSIndexPath indexPath) {

			if(TypeID == 1) {
				Games controller = VC.Storyboard.InstantiateViewController("Games") as Games;
				controller.PlatformID = Models[indexPath.Row].ID;
				controller.PlatformName = Models[indexPath.Row].Name;
				VC.NavigationController.PushViewController(controller, true);
				tableView.DeselectRow(indexPath, true);
			}
			//	UIApplication.SharedApplication.OpenUrl(new NSUrl(AppConstants.GoogleDocsViewer+Downloads[indexPath.Row].Link));
			//	var downloadsVC = (Download)VC.Storyboard.InstantiateViewController ("Download");
			//	downloadsVC.DownloadTypeID = Downloads [indexPath.Row].Type;
			//	downloadsVC.DownloadName = Downloads [indexPath.Row].Title;
			//	VC.NavigationController.PushViewController (downloadsVC, true);
			//	tableView.DeselectRow (indexPath, true);
		}
	}
}