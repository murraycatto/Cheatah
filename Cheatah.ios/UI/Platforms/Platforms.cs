using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using CoreGraphics;
using BigTed;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using Cheatah.shared;

namespace Cheatah.ios{
	partial class Platforms : UITableViewController {
		API api;
		Helper helper;
		UIHelper uihelper;
		nfloat newY;

		nfloat deviceWidth;
		nfloat deviceHeight;
		public Platforms(IntPtr handle) : base(handle) { }
		public override void ViewDidLoad() {
			base.ViewDidLoad();
			this.NavigationController.InteractivePopGestureRecognizer.Enabled = true;
			deviceWidth = View.Bounds.Width;
			deviceHeight = View.Bounds.Height;
			api = new API();
			uihelper = new UIHelper ();
			helper = new Helper();
			uihelper.DismissKeyboardOnBackgroundTap (View);
			this.NavigationController.Title = "Platforms";
			this.Title = "Platforms";
			NavigationController.SetNavigationBarHidden(false, true);
			this.TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
			getPlatforms();
		}
		public async void getPlatforms() {
			var bounds = UIScreen.MainScreen.Bounds;
			LoadingOverlay loadingOverlay = new LoadingOverlay (bounds,"Please wait ...");
			if(String.IsNullOrEmpty(helper.getPlatforms())) {
				View.Add (loadingOverlay);
			} else {
				addPlatforms();
			}
			string message = AppConstants.GenericError;
			try {
				message = await api.GetPlatforms();
			} catch(Exception e) {
				Console.WriteLine(e.Message);
			}
			if(message == "0") {
				loadingOverlay.Hide ();
				addPlatforms();
			} else {
				BTProgressHUD.ShowImage(UIImage.FromBundle("error"), message, 1000);
				loadingOverlay.Hide ();
				this.NavigationController.PopViewController(true);
			}
		}
		void addPlatforms() {
			try {
				List<GenericModel> platforms = new List<GenericModel> { };
				JObject json = JObject.Parse(helper.getPlatforms());
				foreach(JObject j in json["platforms"]) {
					string id = j["id"].ToString();
					string image = j["image"].ToString();
					string name = j["name"].ToString();
					platforms.Add(new GenericModel(name, image, int.Parse(id)));
				}
				GenericTableSource ts = new GenericTableSource(platforms, 1);
				ts.SetupVC(this);
				this.TableView.Source = ts;
				this.TableView.ReloadData();
			} catch(Exception e) {
				Console.WriteLine(e.Message);
				BTProgressHUD.ShowImage(UIImage.FromBundle("error"), AppConstants.GenericError, 500);
			}
		}
	}
}
