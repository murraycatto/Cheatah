﻿using System;

using Foundation;
using UIKit;

namespace Cheatah.ios {
	public partial class GameCell : UITableViewCell {
		public static readonly NSString Key = new NSString("GameCell");
		public static readonly UINib Nib;

		static GameCell() {
			Nib = UINib.FromName("GameCell", NSBundle.MainBundle);
		}

		protected GameCell(IntPtr handle) : base(handle) {
			// Note: this .ctor should not contain any initialization logic.
		}
	}
}
