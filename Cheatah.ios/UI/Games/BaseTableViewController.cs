﻿using System;
using UIKit;
using Foundation;
using CoreGraphics;
using SDWebImage;
using Cheatah.shared;

namespace Cheatah.ios {
	public class BaseTableViewController : UITableViewController {
		protected const string cellIdentifier = "GameCell";
		public BaseTableViewController() {
		}

		public BaseTableViewController(IntPtr handle) : base(handle) {
		}
		public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath) {
			return 100;
		}
		protected void ConfigureCell(UITableViewCell cell, GameModel game, UITableView tableView) {
			CGRect ProfilePicFrame = new CGRect(10, 10, 100 - 20, 100 - 20);
			nfloat newX = ProfilePicFrame.Width + 10;
			CGRect NameFrame = new CGRect(newX + 10, 5, tableView.Frame.Width - newX, 100 / 3);
			UIImageView image = (UIImageView)cell.ViewWithTag(2);
			UILabel name = (UILabel)cell.ViewWithTag(3);
			UILabel name2 = (UILabel)cell.ViewWithTag(4);
			UIImageView line = (UIImageView)cell.ViewWithTag(5);
			if(image == null) {
				image = new UIImageView(ProfilePicFrame);
			}
			if(name == null) {
				name = new UILabel(NameFrame);
			}

			if(line == null) {
				line = new UIImageView(new CGRect(newX + 10, 100 - 2, tableView.Frame.Width - newX - 20, 2));
			}
			image.Tag = 2;
			image.ContentMode = UIViewContentMode.ScaleAspectFit;
			try {
				image.SetImage(
					url: new NSUrl(game.Image)
				);
				// Profile pic
				name.Tag = 3;
				name.Text = game.Name;
				name.Font = AppConstants.Verdana;
				name.Font = name.Font.WithSize(20);
				//				if(String.IsNullOrEmpty(game.Name2)){
				//					CGRect NameFrame2 = new CGRect (newX+12, 100/3, tableView.Frame.Width-newX, 100/3);
				//					if (name2 == null) {
				//						name2 = new UILabel (NameFrame2);
				//					}
				//					name2.Tag = 4;
				//					name2.Text = game.Company;
				//					name2.TextColor =  AppConstants.Primary;
				//					name2.Font = name2.Font.WithSize(15);
				//
				//				}else{
				CGRect NameFrame2 = new CGRect(newX + 10, 100 / 3 - 5, tableView.Frame.Width - newX, 100 / 3);
				if(name2 == null) {
					name2 = new UILabel(NameFrame2);
				}
				name2.Tag = 4;
				name2.Text = game.Name2;
				name2.Font = AppConstants.Verdana;
				name2.Font = name2.Font.WithSize(18);
				CGRect CompanyFrame = new CGRect(newX + 10, 100 / 3 * 2 - 5, tableView.Frame.Width - newX, 100 / 3);
				UILabel company = (UILabel)cell.ViewWithTag(7);
				if(company == null) {
					company = new UILabel(CompanyFrame);
				}
				company.Tag = 7;
				company.Text = game.Company;
				company.Font = AppConstants.FuturaMediumItalic;
				company.Font = company.Font.WithSize(15);
				company.TextColor = AppConstants.Primary;

				cell.AddSubview(company);
				//				}
				//company 
				line.Tag = 5;
				line.BackgroundColor = AppConstants.Primary;
				CGRect ImageArrowFrame = new CGRect(tableView.Frame.Width - 25, 40, 15, 15);
				UIImageView imgArrow = (UIImageView)cell.ViewWithTag(6);
				if(imgArrow == null) {
					imgArrow = new UIImageView(ImageArrowFrame);
					imgArrow.Tag = 6;
					cell.AddSubview(imgArrow);
				}
				imgArrow.ContentMode = UIViewContentMode.ScaleAspectFill;
				imgArrow.Image = UIImage.FromBundle("arrow_right.png");
				cell.AddSubview(image);
				cell.AddSubview(name);
				cell.AddSubview(name2);
				cell.AddSubview(line);
				cell.AddSubview(imgArrow);
			} catch(Exception e) {

			}
		}
		public override void ViewDidLoad() {
			base.ViewDidLoad();
			TableView.RegisterNibForCellReuse(UINib.FromName("GameCell", null), cellIdentifier);
		}

	}
}

