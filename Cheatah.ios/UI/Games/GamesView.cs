using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using CoreGraphics;
using SDWebImage;
using BigTed;
using Newtonsoft.Json.Linq;
using Cheatah.shared;

namespace Cheatah.ios {
	partial class GamesView : UIViewController {
		public GameModel gameModel { get; set; }
		public int PlatformID { get; set; }
		public GamesView(IntPtr handle) : base(handle) { }
		public override void ViewDidAppear(bool animated) {
			base.ViewDidAppear(animated);
			this.NavigationController.NavigationBar.TintColor = AppConstants.Primary;
			UIImage imgAdd = new UIImage("add.png");
			imgAdd = imgAdd.Scale(new CGSize(20,20));
			this.NavigationItem.SetRightBarButtonItem(
				new UIBarButtonItem(imgAdd
				, UIBarButtonItemStyle.Plain
				, (sender, args) => { addFavorite(); })
				, true);
		}
		Helper helper;
		API api;
		UIScrollView scrollView;
		UIHelper uihelper;
		nfloat newY = 0;
		nfloat deviceWidth;
		nfloat deviceHeight;
		public override void ViewDidLoad() {
			base.ViewDidLoad();
			deviceWidth = this.View.Bounds.Width;
			deviceHeight = this.View.Bounds.Height;
			uihelper = new UIHelper ();
			helper = new Helper();
			api = new API();
			this.NavigationController.Title = gameModel.Name;
			this.Title = gameModel.Name;
			scrollView = new UIScrollView(new CGRect(0, 0, deviceWidth, deviceHeight));
			View.Add(scrollView);
			UIButton scrollToTop = new UIButton(new CGRect(deviceWidth - 45, deviceHeight - 45, 40, 40));
			scrollToTop.BackgroundColor = AppConstants.Primary;
			scrollToTop.Layer.ShadowColor = UIColor.DarkGray.CGColor;
			scrollToTop.Layer.ShadowOpacity = 1.0f;
			scrollToTop.Layer.ShadowRadius = 6.0f;
			scrollToTop.Layer.CornerRadius = 20;
			scrollToTop.Layer.ShadowOffset = new System.Drawing.SizeF(0f, 2f);
			scrollToTop.Layer.MasksToBounds = false;
			UIImageView arrow = new UIImageView(new CGRect(10, 8, 20, 20));
			arrow.Image = UIImage.FromBundle("arrow_up.png");
			scrollToTop.Add(arrow);
			View.Add(scrollToTop);
			scrollToTop.TouchUpInside += delegate {
				scrollView.SetContentOffset(new CGPoint(0, 0 - this.NavigationController.NavigationBar.Frame.Height - 15), true);
			};
			#region
			UIImageView banner = new UIImageView(new CGRect(0, newY, deviceWidth, 120)) {
				ContentMode = UIViewContentMode.ScaleAspectFill,
				BackgroundColor = UIKit.UIColor.Clear,
			};
			banner.SetImage(
				url: new NSUrl(gameModel.Banner)
			);
			scrollView.AddSubview(banner);
			newY = uihelper.incNewY(newY,banner);
			newY = newY ;
			#endregion

			#region
			UIImageView spacer = new UIImageView(new CGRect(0, newY, deviceWidth, 20));
			spacer.BackgroundColor = AppConstants.Grey;
			scrollView.AddSubview(spacer);
			newY = uihelper.incNewY (newY, spacer,10);
			UILabel lblName = new UILabel(new CGRect(10, newY, deviceWidth - 20, 30));
			lblName.Text = gameModel.Name;
			lblName.Font = lblName.Font.WithSize(22);
			//lblName.Font = AppConstants.HelveticaNeueBold;
			lblName.TextAlignment = UITextAlignment.Left;
			lblName.TextColor = UIColor.Black;
			UIImageView imgPlatform = new UIImageView(new CGRect(deviceWidth - 50, newY, 40, 40));
			JObject jPlatforms = JObject.Parse(helper.getPlatforms());
			foreach(JObject j in jPlatforms["platforms"]) {
				if(j["id"].ToString() == PlatformID + "") {
					imgPlatform.SetImage(
						url: new NSUrl(j["image"].ToString())
					);
				}
			}
			newY = uihelper.incNewY (newY, lblName);
			scrollView.AddSubview(lblName);
			scrollView.AddSubview(imgPlatform);
			if(!String.IsNullOrEmpty(gameModel.Name2)) {
				UILabel lblName2 = new UILabel(new CGRect(10, newY - 5, deviceWidth - 20, 30));
				lblName2.Text = gameModel.Name2;
				//lblName2.Font = AppConstants.HelveticaNeueMedium;
				lblName2.Font = lblName2.Font.WithSize(18);
				lblName2.TextAlignment = UITextAlignment.Left;
				newY = uihelper.incNewY (newY, lblName2);
				scrollView.AddSubview(lblName2);
			}
			#endregion

			#region
			UILabel lblCompany = new UILabel(new CGRect(10, newY, deviceWidth - 20, 20));
			lblCompany.Text = gameModel.Company;
			//lblCompany.Font = AppConstants.FuturaMediumItalic;
			lblCompany.Font = lblCompany.Font.WithSize(15);
			lblCompany.TextAlignment = UITextAlignment.Left;
			lblCompany.TextColor = AppConstants.Primary;

			newY = uihelper.incNewY (newY, lblCompany,5);
			scrollView.AddSubview(lblCompany);
			#endregion

			#region
			UITextView txInfo = new UITextView(new CGRect(10, newY + 10, deviceWidth - 20, 150));
			uihelper.createInfoView (txInfo,gameModel.Info);
			txInfo.Font = txInfo.Font.WithSize(17);
			scrollView.AddSubview(txInfo);
			newY = uihelper.incNewY(newY,txInfo,20);
			#endregion

			if(String.IsNullOrEmpty(helper.getCheats(PlatformID + "", gameModel.ID + ""))) {
				getCheats();
			} else {
				updateCheats();
				addCheats();
			}
			CGSize screensize = new CGSize(deviceWidth, newY);
			scrollView.ContentSize = screensize;
		}
		public async void addFavorite() {
			var bounds = UIScreen.MainScreen.Bounds;
			LoadingOverlay loadingOverlay = new LoadingOverlay (bounds, "Please wait ...");
			View.Add (loadingOverlay);
			string message = AppConstants.GenericError;
			try {
				message = await api.AddFav(PlatformID, gameModel.ID);
			} catch(Exception e) {
				Console.WriteLine(e.Message);
			}
			if(message != "0") {
				BTProgressHUD.ShowImage(UIImage.FromBundle("error"), message, 1500);
				loadingOverlay.Hide (1.5,true);
			} else {
				BTProgressHUD.ShowImage(UIImage.FromBundle("success"), "Added To Favorites", 1500);
				loadingOverlay.Hide (2, true);
			}
		}
		public async void updateCheats() {
			string message = AppConstants.GenericError;
			try {
				message = await api.GetCheats(PlatformID, gameModel.ID);
			} catch(Exception e) {
				Console.WriteLine(e.Message);
			}
			if(message != "0") {
				BTProgressHUD.ShowImage(UIImage.FromBundle("error"), message, 1000);
			}
		}
		public async void getCheats() {
			var bounds = UIScreen.MainScreen.Bounds;
			LoadingOverlay loadingOverlay = new LoadingOverlay (bounds,"Please wait ...");
			View.Add (loadingOverlay);
			string message = AppConstants.GenericError;
			try {
				message = await api.GetCheats(PlatformID, gameModel.ID);
			} catch(Exception e) {
				Console.WriteLine(e.Message);
			}
			if(message == "0") {
				loadingOverlay.Hide ();
				addCheats();
			} else {
				BTProgressHUD.ShowImage(UIImage.FromBundle("error"), message, 1000);
				loadingOverlay.Hide ();
				this.NavigationController.PopViewController(true);
			}
		}
		void addCheats() {
			try {
				JObject jsonObject = JObject.Parse(helper.getCheats(PlatformID + "", gameModel.ID + ""));
				#region
				foreach(JObject json in jsonObject["types"]) {
					int cheat_count = int.Parse(json["cheat_count"].ToString());
					if(cheat_count > 0) {
						UIImageView spacer2 = new UIImageView(new CGRect(0, newY, deviceWidth, 20));
						spacer2.BackgroundColor = AppConstants.Grey;
						scrollView.AddSubview(spacer2);
						newY = uihelper.incNewY (newY, spacer2,10);
						UILabel lblCheats = new UILabel(new CGRect(0, newY, deviceWidth, 30));
						string type_name = json["type_name"].ToString();
						lblCheats.Text = type_name;
						//lblCheats.Font = AppConstants.HelveticaNeueBold;
						if(type_name.Length > 20) {
							lblCheats.Font = lblCheats.Font.WithSize(20);
						} else {
							lblCheats.Font = lblCheats.Font.WithSize(30);
						}
						lblCheats.TextAlignment = UITextAlignment.Center;
						lblCheats.TextColor = UIColor.Black;
						newY = uihelper.incNewY (newY, lblCheats,10);
						scrollView.AddSubview(lblCheats);
						#endregion
						#region
						UIImageView line3 = new UIImageView(new CGRect(0, newY, deviceWidth, 5));
						line3.BackgroundColor = AppConstants.Primary;
						scrollView.AddSubview(line3);
						newY = newY + 10;
						#endregion
						foreach(JObject j in json["cheats"]) {
							string code = j["code"].ToString();
							string info = j["info"].ToString();
							string name = j["name"].ToString();
							string istext = j["istext"].ToString();
							Console.WriteLine(j.ToString());
							#region
							if(!String.IsNullOrEmpty(name)) {
								UILabel lblName = new UILabel(new CGRect(10, newY, deviceWidth - 20, 30));
								lblName.Text = name;
								//lblName.Font = AppConstants.HelveticaNeueBold;
								if(name.Length < 30) {
									lblName.Font = lblName.Font.WithSize(18);
								} else {
									lblName.Font = lblName.Font.WithSize(15);
								}
								lblName.TextAlignment = UITextAlignment.Left;
								lblName.TextColor = AppConstants.Secondary;
								newY = uihelper.incNewY (newY, lblName);
								newY = newY + 10;
								scrollView.AddSubview(lblName);
							} else {
								newY = newY + 10;
							}
							#endregion
							#region
							if(!String.IsNullOrEmpty(info)) {
								newY = newY - 10;
								UITextView txtInfo;
								if(!String.IsNullOrEmpty(code)) {
									if(info.Length > 440) {
										txtInfo = new UITextView(new CGRect(8, newY, deviceWidth - 20, 180));
									} else if(info.Length > 340) {
										txtInfo = new UITextView(new CGRect(8, newY, deviceWidth - 20, 140));
									} else if(info.Length > 240) {
										txtInfo = new UITextView(new CGRect(8, newY, deviceWidth - 20, 120));
									} else if(info.Length > 140) {
										txtInfo = new UITextView(new CGRect(8, newY, deviceWidth - 20, 80));
									} else {
										txtInfo = new UITextView(new CGRect(8, newY, deviceWidth - 20, 60));
									}
								} else {
									txtInfo = new UITextView(new CGRect(10, newY, deviceWidth - 20, 150));
								}

								txtInfo.Text = info;
								//txtInfo.Font = AppConstants.HelveticaNeueMedium;
								txtInfo.Font = txtInfo.Font.WithSize(13);
								txtInfo.TextAlignment = UITextAlignment.Left;
								txtInfo.Editable = false;
								txtInfo.TextColor = AppConstants.Secondary;
								newY = uihelper.incNewY (newY, txtInfo);
								scrollView.AddSubview(txtInfo);
							}
							#endregion
							#region
							if(!String.IsNullOrEmpty(code)) {
								if(istext == "0") {
									string[] codes = code.Split(',');
									nfloat newX = 10;
									int count = 1;
									foreach(string s in codes) {
										UIImageView imgBtn = new UIImageView(new CGRect(newX, newY, 30, 30));
										imgBtn.ContentMode = UIViewContentMode.ScaleAspectFit;
										newX += 35;
										imgBtn.Image = UIImage.FromBundle(s.Trim().ToLower() + ".png");
										scrollView.AddSubview(imgBtn);
										if(newX >= deviceWidth - 30) {
											if(codes.Length != count) {
												newY += 40;
											}
											newX = 10;
										}
										count++;
									}
									newY += 40;
								} else {
									UITextView txtCode;
									if(code.Length > 440) {
										txtCode = new UITextView(new CGRect(8, newY, deviceWidth - 20, 180));
									} else if(code.Length > 340) {
										txtCode = new UITextView(new CGRect(8, newY, deviceWidth - 20, 140));
									} else if(code.Length > 240) {
										txtCode = new UITextView(new CGRect(8, newY, deviceWidth - 20, 120));
									} else if(code.Length > 140) {
										txtCode = new UITextView(new CGRect(8, newY, deviceWidth - 20, 80));
									} else {
										txtCode = new UITextView(new CGRect(8, newY, deviceWidth - 20, 60));
									}
									txtCode.Text = code;
									txtCode.Font = AppConstants.Verdana;
									if(code.Length < 50) {
										txtCode.Font = txtCode.Font.WithSize(15);
									} else {
										txtCode.Font = txtCode.Font.WithSize(12);
									}
									txtCode.TextAlignment = UITextAlignment.Left;
									txtCode.Editable = false;
									newY = uihelper.incNewY (newY, txtCode,5);
									scrollView.AddSubview(txtCode);
								}
							}
							UIImageView line = new UIImageView(new CGRect(0, newY, deviceWidth, 5));
							line.BackgroundColor = AppConstants.Primary;
							scrollView.AddSubview(line);
							newY = newY + 5;
							CGSize screensize = new CGSize(deviceWidth, newY);
							scrollView.ContentSize = screensize;
							#endregion
						}
					}
				}



			} catch(Exception e) {
				Console.WriteLine(e.Message);
				BTProgressHUD.ShowImage(UIImage.FromBundle("error"), AppConstants.GenericError, 500);
			}
		}

	}
}
