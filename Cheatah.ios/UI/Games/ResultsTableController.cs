﻿using System;

using Foundation;
using UIKit;
using System.Collections.Generic;
using CoreGraphics;
using Cheatah.shared;

namespace Cheatah.ios {
	public class ResultsTableController : BaseTableViewController {
		public List<GameModel> FilteredGames { get; set; }
		public override nint RowsInSection(UITableView tableview, nint section) {
			return FilteredGames.Count;
		}
		public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath) {
			return 100;
		}
		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath) {
			GameModel game = FilteredGames[indexPath.Row];
			UITableViewCell cell = tableView.DequeueReusableCell(cellIdentifier);
			tableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
			ConfigureCell(cell, game, tableView);
			return cell;
		}

	}
}
