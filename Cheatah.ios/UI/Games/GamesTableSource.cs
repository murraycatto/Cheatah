﻿using System;
using UIKit;
using Foundation;
using System.Collections.Generic;
using CoreGraphics;
using Cheatah.shared;
using System.Drawing;
using SDWebImage;

namespace Cheatah.ios
{
	public class GamesTableSource : UITableViewSource
	{

		List<GameModel> Games;
		string CellIdentifier = "GameCell";
		Home VC;

		public GamesTableSource(List<GameModel> games, Home vc)
		{
			Games = games;
			VC = vc;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return Games.Count;
		}

		public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			return 100;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = tableView.DequeueReusableCell(CellIdentifier);
			GameModel game = Games[indexPath.Row];
			//---- if there are no cells to reuse, create a new one
			if (cell == null)
			{
				cell = new UITableViewCell(new CGRect(0, 0, tableView.Frame.Width, 100));
				cell.RestorationIdentifier = CellIdentifier;
			}
			CGRect ProfilePicFrame = new CGRect(10, 10, 100 - 20, 100 - 20);
			CGRect PlatformPicFrame = new CGRect(tableView.Frame.Width - 30, 5, 20, 20);
			nfloat newX = ProfilePicFrame.Width + 10;
			CGRect NameFrame = new CGRect(newX + 10, 5, tableView.Frame.Width - newX, 100 / 3);
			UIImageView image = (UIImageView)cell.ViewWithTag(2);
			UILabel name = (UILabel)cell.ViewWithTag(3);
			UILabel name2 = (UILabel)cell.ViewWithTag(4);
			UIImageView line = (UIImageView)cell.ViewWithTag(5);
			UIImageView platform = (UIImageView)cell.ViewWithTag(8);
			if (image == null)
			{
				image = new UIImageView(ProfilePicFrame);
			}
			if (platform == null)
			{
				platform = new UIImageView(PlatformPicFrame);
			}
			if (name == null)
			{
				name = new UILabel(NameFrame);
			}
			if (line == null)
			{
				line = new UIImageView(new CGRect(newX + 10, 100 - 2, tableView.Frame.Width - newX - 20, 2));
			}
			image.Tag = 2;
			image.ContentMode = UIViewContentMode.ScaleAspectFit;
			image.SetImage(
				url: new NSUrl(game.Image)
			);
			// Profile pic
			name.Tag = 3;
			name.Text = game.Name;
			name.Font = name.Font.WithSize(20);
			//name.Font = AppConstants.HelveticaNeueBold;
			//if(String.IsNullOrEmpty(game.Name2)){
			//	CGRect NameFrame2 = new CGRect (newX+12, 100/3, tableView.Frame.Width-newX, 100/3);
			//	if (name2 == null) {
			//		name2 = new UILabel (NameFrame2);
			//	}
			//	name2.Tag = 4;
			//	name2.Text = game.Company;
			//	name2.TextColor =  AppConstants.Primary;
			//	name2.Font = name2.Font.WithSize(15);

			//}else{
			CGRect NameFrame2 = new CGRect(newX + 10, 100 / 3 - 5, tableView.Frame.Width - newX, 100 / 3);
			if (name2 == null)
			{
				name2 = new UILabel(NameFrame2);
			}
			name2.Tag = 4;
			name2.Text = game.Name2;
			//name2.Font = AppConstants.HelveticaNeueMedium;
			name2.Font = name2.Font.WithSize(18);
			CGRect CompanyFrame = new CGRect(newX + 10, 100 / 3 * 2 - 5, tableView.Frame.Width - newX, 100 / 3);
			UILabel company = (UILabel)cell.ViewWithTag(7);
			if (company == null)
			{
				company = new UILabel(CompanyFrame);
			}
			company.Tag = 7;
			company.Text = game.Company;
			//company.Font = AppConstants.FuturaMediumItalic;
			company.Font = company.Font.WithSize(15);
			company.TextColor = AppConstants.Primary;

			cell.AddSubview(company);
			//				}
			//company 
			line.Tag = 5;
			line.BackgroundColor = AppConstants.Primary;
			CGRect ImageArrowFrame = new CGRect(tableView.Frame.Width - 25, 40, 15, 15);
			UIImageView imgArrow = (UIImageView)cell.ViewWithTag(6);
			if (imgArrow == null)
			{
				imgArrow = new UIImageView(ImageArrowFrame);
				imgArrow.Tag = 6;
				cell.AddSubview(imgArrow);
			}
			imgArrow.ContentMode = UIViewContentMode.ScaleAspectFill;
			imgArrow.Image = UIImage.FromBundle("arrow_right.png");

			platform.ContentMode = UIViewContentMode.ScaleAspectFill;
			platform.SetImage(
				url: new NSUrl(game.PlatformImage)
			);
			cell.AddSubview(image);
			cell.AddSubview(name);
			cell.AddSubview(name2);
			cell.AddSubview(line);
			cell.AddSubview(imgArrow);
			cell.AddSubview(platform);

			return cell;
		}

		public override void CommitEditingStyle(UITableView tableView, UITableViewCellEditingStyle editingStyle, Foundation.NSIndexPath indexPath)
		{
			switch (editingStyle)
			{
				case UITableViewCellEditingStyle.Delete:
					// remove the item from the underlying data source
					GameModel gm = Games[indexPath.Row];
					Games.RemoveAt(indexPath.Row);
					// delete the row from the table
					tableView.DeleteRows(new NSIndexPath[] { indexPath }, UITableViewRowAnimation.Fade);
					VC.DeleteGame(gm);

					break;
				case UITableViewCellEditingStyle.None:
					Console.WriteLine("CommitEditingStyle:None called");
					break;
			}
		}
		public override bool CanEditRow(UITableView tableView, NSIndexPath indexPath)
		{
			return true; // return false if you wish to disable editing for a specific indexPath or for all rows
		}
		public override string TitleForDeleteConfirmation(UITableView tableView, NSIndexPath indexPath)
		{   // Optional - default text is 'Delete'
			return "Delete";
		}
		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			GamesView controller = VC.Storyboard.InstantiateViewController("GamesView") as GamesView;
			controller.gameModel = Games[indexPath.Row];
			controller.PlatformID = Games[indexPath.Row].PlatformID;
			VC.NavigationController.PushViewController(controller, true);
			tableView.DeselectRow(indexPath, true);
		}
	}
}