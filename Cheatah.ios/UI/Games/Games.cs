using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using CoreGraphics;
using System.Collections.Generic;
using System.Linq;
using Cheatah.shared;
using BigTed;
using Newtonsoft.Json.Linq;

namespace Cheatah.ios{
	partial class Games : BaseTableViewController {
		UIScrollView scrollView;
		UIHelper uihelper;
		Helper helper;
		API api;
		public int PlatformID { get; set; }
		public string PlatformName { get; set; }
		ResultsTableController resultsTableController;
		UISearchController searchController;
		bool searchControllerWasActive;
		bool searchControllerSearchFieldWasFirstResponder;
		GameModel[] games;

		nfloat newY;
		nfloat deviceWidth;
		nfloat deviceHeight;

		public Games(IntPtr handle) : base(handle) {
			games = new GameModel[0];
		}

		public override void ViewDidLoad() {
			base.ViewDidLoad();
			deviceWidth = View.Bounds.Width;
			deviceHeight = View.Bounds.Height;
			api = new API();
			uihelper = new UIHelper();
			uihelper.LogFonts();
			helper = new Helper();
			this.Title = PlatformName + " Games";
			this.TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
			uihelper.DismissKeyboardOnBackgroundTap(View);
			NavigationController.NavigationBar.Translucent = true;
			NavigationController.SetNavigationBarHidden(false, true);
			resultsTableController = new ResultsTableController {
				FilteredGames = new List<GameModel>()
			};
			searchController = new UISearchController(resultsTableController) {
				WeakDelegate = this,
				DimsBackgroundDuringPresentation = false,
				WeakSearchResultsUpdater = this
			};
			searchController.SearchBar.SizeToFit();
			TableView.TableHeaderView = searchController.SearchBar;
			resultsTableController.TableView.WeakDelegate = this;
			searchController.SearchBar.WeakDelegate = this;
			DefinesPresentationContext = true;
			if(searchControllerWasActive) {
				searchController.Active = searchControllerWasActive;
				searchControllerWasActive = false;

				if(searchControllerSearchFieldWasFirstResponder) {
					searchController.SearchBar.BecomeFirstResponder();
					searchControllerSearchFieldWasFirstResponder = false;
				}
			}
			if(String.IsNullOrEmpty(helper.getGames(PlatformID + ""))) {
				getGames();
			} else {
				addGames(helper.getGames(PlatformID + ""));
				updateGames();
			}
		}
		public void addGames(string List) {
			try {
				JObject json = JObject.Parse(List);
				int count = 0;
				foreach(JObject j in json["games"]) {
					count++;
				}
				games = new GameModel[count];
				count = 0;
				foreach(JObject j in json["games"]) {
					games[count] = new GameModel(int.Parse(j["id"].ToString()), j["name"].ToString(), j["name2"].ToString(), j["image"].ToString(), j["banner"].ToString(), j["info"].ToString(), j["company"].ToString());
					count++;
				}
				this.TableView.ReloadData();
			} catch(Exception e) {
				Console.WriteLine(e.Message);
				BTProgressHUD.ShowImage(UIImage.FromBundle("error"), AppConstants.GenericError, 500);
			}
		}
		public async void updateGames() {
			string message = AppConstants.GenericError;
			try {
				message = await api.GetGames(PlatformID);
				addGames(helper.getGames(PlatformID + ""));
			} catch(Exception e) {
				Console.WriteLine(e.Message);
			}
			if(message == "0") {
			} else {
				var bounds = UIScreen.MainScreen.Bounds;
				LoadingOverlay loadingOverlay = new LoadingOverlay (bounds," ");
				View.Add (loadingOverlay);
				BTProgressHUD.ShowImage(UIImage.FromBundle("error"), message, 1000);
				loadingOverlay.Hide (2, true);
			}
		}
		public async void getGames() {
			var bounds = UIScreen.MainScreen.Bounds;
			LoadingOverlay loadingOverlay = new LoadingOverlay (bounds,"Please wait ...");
			View.Add (loadingOverlay);
			string message = AppConstants.GenericError;

			try {
				message = await api.GetGames(PlatformID);
				addGames(helper.getGames(PlatformID + ""));
			} catch(Exception e) {
				Console.WriteLine(e.Message);
			}
			if(message == "0") {
				loadingOverlay.Hide ();
			} else {
				BTProgressHUD.ShowImage(UIImage.FromBundle("error"), message, 1000);
				loadingOverlay.Hide (2, true);
				startDashboard();
			}
		}
		public void startDashboard() {
			NavigationController.PopViewController(true);
		}
		[Export("searchBarSearchButtonClicked:")]
		public virtual void SearchButtonClicked(UISearchBar searchBar) {
			Console.WriteLine("");
			searchBar.ResignFirstResponder();
		}

		public override nint RowsInSection(UITableView tableview, nint section) {
			return games.Length;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath) {
			GameModel game = games[indexPath.Row];
			UITableViewCell cell = TableView.DequeueReusableCell((NSString)cellIdentifier, indexPath);
			ConfigureCell(cell, game, tableView);
			return cell;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath) {
			GameModel selected = (tableView == TableView) ? games[indexPath.Row] : resultsTableController.FilteredGames[indexPath.Row];

			var gameView = (GamesView)Storyboard.InstantiateViewController("GamesView");
			gameView.gameModel = selected;
			gameView.PlatformID = PlatformID;
			NavigationController.PushViewController(gameView, true);
			tableView.DeselectRow(indexPath, true);
		}

		[Export("tableView:heightForRowAtIndexPath:")]
		public nfloat CustomGetHeightForRow(UITableView tableView, IntPtr indexPath) {
			return 100;
		}
		[Export("updateSearchResultsForSearchController:")]
		public virtual void UpdateSearchResultsForSearchController(UISearchController searchController) {
			var tableController = (ResultsTableController)searchController.SearchResultsController;
			tableController.FilteredGames = PerformSearch(searchController.SearchBar.Text);
			tableController.TableView.ReloadData();
		}

		List<GameModel> PerformSearch(string searchString) {
			searchString = searchString.Trim();
			string[] searchItems = string.IsNullOrEmpty(searchString)
				? new string[0]
				: searchString.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

			var filteredgames = new List<GameModel>();

			foreach(var item in searchItems) {

				IEnumerable<GameModel> query =
					from c in games
					where c.Name.IndexOf(item, StringComparison.OrdinalIgnoreCase) >= 0
					|| c.Company.IndexOf(item, StringComparison.OrdinalIgnoreCase) >= 0
					//					orderby c.Rank
					select c;
				filteredgames.AddRange(query);
			}
			return filteredgames.Distinct().ToList();
		}
	}
}
