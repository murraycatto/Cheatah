﻿using System;
using System.Text;
using Cheatah.shared;
using UIKit;

namespace Cheatah.ios {
	public class UIHelper {
		
		public UIHelper() {
		}
		public nfloat incNewY(nfloat newY, UIView view, int extra = 0) {
			return newY + view.Bounds.Height + extra;
		}
		public UITextView createInfoView(UITextView txt, string defaultText = "") {
			txt.Text = defaultText;
			txt.UserInteractionEnabled = true;
			txt.Editable = false;
			txt.Font = AppConstants.Verdana;
			txt.ScrollEnabled = true;
			txt.TextAlignment = UITextAlignment.Left;
			txt.Font = txt.Font.WithSize(15);
			return txt;
		}
		// Utilities
		public void DismissKeyboardOnBackgroundTap(UIView View) {
			var tap = new UITapGestureRecognizer { CancelsTouchesInView = false };
			tap.AddTarget(() => View.EndEditing(true));
			View.AddGestureRecognizer(tap);
		}

		public void ClearViews(UIView view) {
			foreach(UIView v in view) {
				v.RemoveFromSuperview();
			}
		}
		public void LogFonts() {
			var fontList = new StringBuilder();
			var familyNames = UIFont.FamilyNames;
			foreach(var familyName in familyNames) {
				fontList.Append(String.Format("Family: {0}\n", familyName));
				Console.WriteLine("Family: {0}\n", familyName);
				var fontNames = UIFont.FontNamesForFamilyName(familyName);
				foreach(var fontName in fontNames) {
					Console.WriteLine("\tFont: {0}\n", fontName);
					fontList.Append(String.Format("\tFont: {0}\n", fontName));
				}
			};
		}
	}
}
