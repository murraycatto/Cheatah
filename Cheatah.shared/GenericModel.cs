﻿using System;
namespace Cheatah.shared
{
	public class GenericModel
	{
		public string Name;
		public string Image;
		public int ID;

		public GenericModel(string Link, string Image, int ID)
		{
			this.Name = Link;
			this.Image = Image;
			this.ID = ID;
		}
	}
}
