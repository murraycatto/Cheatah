﻿using System;

#if __ANDROID__
using Android.Content;
using Android.Preferences;
using Newtonsoft.Json.Linq;

#else
using Foundation;
using Newtonsoft.Json.Linq;
using UIKit;
#endif

namespace Cheatah.shared
{
	public class Helper
	{
#if __ANDROID__
		Context context;
		public Helper (Context c){
			context = c;
		}
		public void setPref(string key, string value){
			ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(context); 
			ISharedPreferencesEditor editor = prefs.Edit();
			editor.PutString(key, value);
			editor.Apply();
		}
		public string getPref(string key){
			ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences (context);
			return prefs.GetString (key,"");
		}
#else
		public Helper()
		{
		}

		public void setPref(string key, string value)
		{
			NSUserDefaults.StandardUserDefaults.SetString(value, key);
			NSUserDefaults.StandardUserDefaults.Synchronize();
		}

		public string getPref(string key)
		{
			return NSUserDefaults.StandardUserDefaults.StringForKey(key);
		}
#endif
		public string getOS()
		{
#if __ANDROID__
			return "android";
#else
			return "ios";
#endif
		}

		public string getUUID()
		{
			return getPref("UUID");
		}
		public void setUUID(string UUID)
		{
			setPref("UUID", UUID);
		}

		public string getBanners()
		{
			return getPref("Banners");
		}
		public void setBanners(string Banners)
		{
			setPref("Banners", Banners);
		}

		public string getFavorite()
		{
			return getPref("Favorite");
		}
		public void setFavorite(string Favorite)
		{
			setPref("Favorite", Favorite);
		}


		public string getPlatforms()
		{
			return getPref("Platforms");
		}
		public void setPlatforms(string Platforms)
		{
			setPref("Platforms", Platforms);
		}

		public string getGames(string PlatformID)
		{
			return getPref("Games" + PlatformID);
		}
		public void setGames(string Games, string PlatformID)
		{
			setPref("Games" + PlatformID, Games);
		}


		public string getCheats(string PlatformID, string GameID)
		{
			return getPref("Cheats" + PlatformID + "_" + GameID);
		}
		public void setCheats(string Cheats, string PlatformID, string GameID)
		{
			setPref("Cheats" + PlatformID + "_" + GameID, Cheats);
		}

	}
}

