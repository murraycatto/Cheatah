﻿using System;
using RestSharp;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
#if __ANDROID__
using Android.Content;
#endif
namespace Cheatah.shared
{
	public class API
	{
		Helper helper;
#if __ANDROID__
		public API (Context c)
		{
			helper = new Helper (c);
		}
#else
		public API()
		{
			helper = new Helper();
		}
#endif

		public async Task<string> GetGames(int PlatformID)
		{
			try
			{
				RestClient client = new RestClient(AppConstants.APIURL + "get_games.cfm");
				var request = new RestRequest("", Method.POST);
				request.AddParameter("token", AppConstants.Token);
				request.AddParameter("os", AppConstants.OS);
				request.AddParameter("PlatformID", PlatformID);
				var response = await client.ExecuteTaskAsync(request);
				return ProccessResponse(response.StatusCode.ToString(), response.Content, "get_games");
			}
			catch (Exception e)
			{
				if (AppConstants.Mode == "Beta")
				{
					Console.WriteLine("API Error:" + e.Message);
				}
				return AppConstants.GenericError;
			}
		}
		public async Task<string> GetCheats(int PlatformID, int GameID)
		{
			try
			{
				RestClient client = new RestClient(AppConstants.APIURL + "get_cheats.cfm");
				var request = new RestRequest("", Method.POST);
				request.AddParameter("token", AppConstants.Token);
				request.AddParameter("os", AppConstants.OS);
				request.AddParameter("PlatformID", PlatformID);
				request.AddParameter("GameID", GameID);
				request.AddParameter("UUID", helper.getUUID());
				var response = await client.ExecuteTaskAsync(request);
				return ProccessResponse(response.StatusCode.ToString(), response.Content, "get_cheats");
			}
			catch (Exception e)
			{
				if (AppConstants.Mode == "Beta")
				{
					Console.WriteLine("API Error:" + e.Message);
				}
				return AppConstants.GenericError;
			}
		}

		public async Task<string> AddFav(int PlatformID, int GameID)
		{
			try
			{
				RestClient client = new RestClient(AppConstants.APIURL + "add_favorite.cfm");
				var request = new RestRequest("", Method.POST);
				request.AddParameter("token", AppConstants.Token);
				request.AddParameter("os", AppConstants.OS);
				request.AddParameter("UUID", helper.getUUID());
				request.AddParameter("PlatformID", PlatformID);
				request.AddParameter("GameID", GameID);
				var response = await client.ExecuteTaskAsync(request);
				return ProccessResponse(response.StatusCode.ToString(), response.Content, "add_favorite");
			}
			catch (Exception e)
			{
				if (AppConstants.Mode == "Beta")
				{
					Console.WriteLine("API Error:" + e.Message);
				}
				return AppConstants.GenericError;
			}
		}

		public async Task<string> DeleteFav(int PlatformID, int GameID)
		{
			try
			{
				RestClient client = new RestClient(AppConstants.APIURL + "delete_favorite.cfm");
				var request = new RestRequest("", Method.POST);
				request.AddParameter("token", AppConstants.Token);
				request.AddParameter("os", AppConstants.OS);
				request.AddParameter("PlatformID", PlatformID);
				request.AddParameter("GameID", GameID);
				request.AddParameter("UUID", helper.getUUID());
				var response = await client.ExecuteTaskAsync(request);
				return ProccessResponse(response.StatusCode.ToString(), response.Content, "delete_favorite");
			}
			catch (Exception e)
			{
				if (AppConstants.Mode == "Beta")
				{
					Console.WriteLine("API Error:" + e.Message);
				}
				return AppConstants.GenericError;
			}
		}

		public async Task<string> GetFav()
		{
			try
			{
				RestClient client = new RestClient(AppConstants.APIURL + "get_favorites.cfm");
				var request = new RestRequest("", Method.POST);
				request.AddParameter("token", AppConstants.Token);
				request.AddParameter("os", AppConstants.OS);
				request.AddParameter("UUID", helper.getUUID());
				var response = await client.ExecuteTaskAsync(request);
				return ProccessResponse(response.StatusCode.ToString(), response.Content, "get_favorites");
			}
			catch (Exception e)
			{
				if (AppConstants.Mode == "Beta")
				{
					Console.WriteLine("API Error:" + e.Message);
				}
				return AppConstants.GenericError;
			}
		}

		public async Task<string> GetBanners()
		{
			try
			{
				RestClient client = new RestClient(AppConstants.APIURL + "get_banners.cfm");
				var request = new RestRequest("", Method.POST);
				request.AddParameter("token", AppConstants.Token);
				request.AddParameter("os", AppConstants.OS);
				var response = await client.ExecuteTaskAsync(request);
				return ProccessResponse(response.StatusCode.ToString(), response.Content, "get_banners");
			}
			catch (Exception e)
			{
				if (AppConstants.Mode == "Beta")
				{
					Console.WriteLine("API Error:" + e.Message);
				}
				return AppConstants.GenericError;
			}
		}

		public async Task<string> GetPlatforms()
		{
			try
			{
				RestClient client = new RestClient(AppConstants.APIURL + "get_platforms.cfm");
				var request = new RestRequest("", Method.POST);
				request.AddParameter("token", AppConstants.Token);
				request.AddParameter("os", AppConstants.OS);
				var response = await client.ExecuteTaskAsync(request);
				return ProccessResponse(response.StatusCode.ToString(), response.Content, "get_platforms");
			}
			catch (Exception e)
			{
				if (AppConstants.Mode == "Beta")
				{
					Console.WriteLine("API Error:" + e.Message);
				}
				return AppConstants.GenericError;
			}
		}




		public string ProccessResponse(string StatusCode, string Content, string callName = "")
		{
			try
			{
				if (AppConstants.Mode == "Beta")
				{
					Console.WriteLine("API Response:");
					Console.WriteLine("  Content: " + Content);
					Console.WriteLine("  Status code: " + StatusCode);
				}
				if (StatusCode == "0")
				{
					return AppConstants.InternetError;
				}
				JObject json = JObject.Parse(Content);
				if (json["status"].ToString() == "1")
				{
					return json["message"].ToString();
				}
				else if (json["status"].ToString() == "0")
				{
					Console.WriteLine("Call Name:" + callName);
					Console.WriteLine("API is Valid Json");
					switch (callName)
					{
						case "get_platforms":
							helper.setPlatforms(json.ToString());
							break;
						case "get_banners":
							helper.setBanners(json.ToString());
							break;
						case "get_favorites":
							helper.setFavorite(json.ToString());
							break;
						case "get_games":
							helper.setGames(json.ToString(),json["platformid"].ToString());
							break;
						case "get_cheats":
							helper.setCheats(json.ToString(),json["platformid"].ToString(),json["gameid"].ToString());
						break;
						default:
							break;
					}
					return "0";
				}
				else {
					return AppConstants.GenericError;
				}
			}
			catch (Exception e)
			{
				if (AppConstants.Mode == "Beta")
				{
					Console.WriteLine("API Response ERROR!");
					Console.WriteLine(e.Message);
					Console.WriteLine("------------------");
				}
				return AppConstants.GenericError;
			}
		}
	}
}

