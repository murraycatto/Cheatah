﻿using System;
namespace Cheatah.shared
{
	public class GameModel
	{
		public int ID;
		public int PlatformID;
		public string Name;
		public string Name2;
		public string Image;
		public string PlatformImage;
		public string PlatformName;
		public string Banner;
		public string Info;
		public string Company;

		public GameModel(int id, string name, string name2, string image, string banner, string info, string company, int platformid = 0, string platform_name = "", string platform_image = "")
		{
			this.ID = id;
			this.Name = name;
			this.Name2 = name2;
			this.Image = image;
			this.Banner = banner;
			this.Info = info;
			this.Company = company;
			this.PlatformID = platformid;
			this.PlatformName = platform_name;
			this.PlatformImage = platform_image;
		}
	}
}
