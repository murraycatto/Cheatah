﻿using System;
#if __ANDROID__
using Android.Graphics;
#else
using UIKit;
#endif
namespace Cheatah.shared
{
	public class AppConstants
	{
		//Version and API
		public static string Version { get; } = "1.0";
#if DEBUG
		public static string Mode { get; } = "Beta";
		public static string URL { get; } = "http://fusebox.co.za/apps/cheatr";
		public static string Token { get; } = "CHEATR-JAHSGDHJAS-YAGSBAS-NABSD";
#else
			public static string Mode { get; } = "Live";
			public static string URL { get; } = "http://traumacall.fusebox.co.za";
			public static string Token { get; } = "TRAUM-CALL-2016-HASH-CODE";
			//public static string URL { get; } = "http://www.traumacall.co.za";
			//public static string Token { get; } = "TRAUM-CALL-2016-HASH-CODE-LIVE";
#endif

		public static string APIURL { get; } = URL + "/api/";

		// Errors
		public static string AuthError { get; } = "Email or password changed you have been logged out.";
		public static string GenericError { get; } = "Error. Please try again later";
		public static string BookingCancled { get; } = "Unfortunately this booking has been canceled. Please book another counselor or call the call center";
		public static string BookingDeclined { get; } = "Sorry, your selected counsellor has become unavailable. Please select a new counsellor";
		public static string NoNewsletter { get; } = "Unfortunately there are no newsletters yet.\n Please check again later";
		public static string NewsletterError { get; } = "Unfortunately an error occured retrieving our newsletters.\n Please check your connection and try again later ";
		public static string TermsError { get; } = "Please accept the terms and conditions.";
		public static string InternetError { get; } = "Internet connectivity issue";
		public static string EmptyError { get; } = "Please enter in your details correctly.";
		public static string LocationError { get; } = "Unable to fetch location";

		//Success Messages


		//Colors and Fonts
#if __ANDROID__
		public static string OS{get;} = "Android";
		public static Color PrimayBlue { get; } = Color.Rgb(0, 148, 249);
		public static Color PrimayGreen { get; } = Color.Rgb(66, 197, 29);
		public static Color PrimayRed { get; } = Color.Rgb(234, 65, 44);
		public static Color MenuPrimary { get; } = Color.Rgb(41, 59, 76);
		public static Color MenuSelected { get; } = Color.Rgb(32, 46, 58);
		public static Color BarGray { get; } = Color.Rgb(244, 244, 244);
		public static Color BarGrayDarker { get; } = Color.Rgb(224, 223, 222);
		public static Color TextGray { get; } = Color.Rgb(193, 193, 193);
		public static Color TextGray2 { get; } = Color.Rgb(155, 155, 155);
#else
		public static string OS { get; } = "IOS";
		public static UIColor Primary { get; } = UIColor.FromRGB(219, 58, 54);
		public static UIColor Secondary { get; } = UIColor.FromRGB(64, 64, 64);
		public static UIColor Grey { get; } = UIColor.FromRGB(219, 219, 219);

		public static UIFont VerdanaItalic { get; } = UIFont.FromName("Verdana-Italic", 20f);
		public static UIFont Verdana { get; } = UIFont.FromName("Verdana", 20f);
		public static UIFont VerdanaBold { get; } = UIFont.FromName("Verdana-Bold", 20f);
		public static UIFont FuturaMediumItalic { get; } = UIFont.FromName("Futura-MediumItalic", 20f);
#endif
		// APP copy 

		public AppConstants() { }
	}
}

